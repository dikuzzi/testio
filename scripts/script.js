'use strict'

let oneNumber = +prompt('Введите первое число');
let twoNumber = +prompt('Введите второе число');

oneNumber = +oneNumber.toFixed(1);
twoNumber = +twoNumber.toFixed(1);

let sum = +(oneNumber + twoNumber).toFixed(2);
let difference = +(oneNumber - twoNumber).toFixed(2);
let division = +(oneNumber / twoNumber).toFixed(2);
let multiply = +(oneNumber * twoNumber).toFixed(2);
let remains = +(oneNumber % twoNumber).toFixed(2);

console.log(`${oneNumber} + ${twoNumber} = ${sum}`);
console.log(`${oneNumber} - ${twoNumber} = ${difference}`);
console.log(`${oneNumber} / ${twoNumber} = ${division}`);
console.log(`${oneNumber} * ${twoNumber} = ${multiply}`);
console.log(`${oneNumber} % ${twoNumber} = ${remains}`);